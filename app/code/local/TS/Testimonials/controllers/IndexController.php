<?php

class TS_Testimonials_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() {

        $this->loadLayout();

        $head = $this->getLayout()->getBlock('head');
        if ($head) {
            $head->setTitle(Mage::getStoreConfig('general/backend/testimonials_title'));
            $head->setDescription(Mage::getStoreConfig('general/backend/testimonials_description'));
            $head->setKeywords(Mage::getStoreConfig('general/backend/testimonials_keywords'));
        }

        $this->_initLayoutMessages('core/session');
        $this->renderLayout();
    }

    public function newAction() {

        if (!Mage::getSingleton("customer/session")->isLoggedIn()) {
            Mage::getSingleton('core/session')->addError('You must login for adding testimonial');

            $this->_redirect('testimonials');
        }

        $this->loadLayout();
        $this->renderLayout();
    }

    public function createAction() {

        if (count($this->getRequest()->getPost()) < 1 || !Mage::getSingleton("customer/session")->isLoggedIn()) {
            $this->_redirectUrl('testimonials');
        }

        $params = $this->getRequest()->getPost();

        $testimonials = Mage::getModel('tstestimonials/testimonials');
        $testimonials->setContent($params['content']);
        $testimonials->setUserId(Mage::getSingleton("customer/session")->getId());


        Mage::getSingleton('core/session')->addNotice("Testimonial save");


        if ($testimonials->save()) {
            $this->_redirect('testimonials');
        } else {
            $this->_redirect('testimonials/index/new');
        }
    }

}

