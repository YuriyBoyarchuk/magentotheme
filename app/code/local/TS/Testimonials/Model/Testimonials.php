<?php

class TS_Testimonials_Model_Testimonials extends Mage_Core_Model_Abstract
{

    public function _construct() {
        $this->_init('tstestimonials/testimonials');
    }

    public function getUserName() {
        return Mage::getModel('customer/customer')->load($this->getUserId())->getName();
    }
}