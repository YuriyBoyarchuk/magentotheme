<?php
$installer = $this;
$tableTestimonials = $installer->getTable('tstestimonials/items');

$installer->startSetup();

$installer->getConnection()->dropTable($tableTestimonials);
$table = $installer->getConnection()
    ->newTable($tableTestimonials)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ))
    ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false
    ))
    ->addColumn('content', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
        'default'   => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE
    ));
$installer->getConnection()->createTable($table);

$installer->endSetup();