<?php

$installer = $this;
$tableTestimonials = $installer->getTable('tstestimonials/items');
$installer->startSetup();

$installer->getConnection()
    ->addColumn($tableTestimonials,
        'show_status',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
            'default' => 0,
            'nullable'  => false,
            'comment'   => 'Show Status'
        )
    );

$installer->getConnection()
    ->addColumn($tableTestimonials, 'updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
        array('nullable'  => false)
    );

$installer->getConnection()
    ->addForeignKey(
        $this->getFkName('customer/entity', 'entity_id', $tableTestimonials, 'user_id'),
        $tableTestimonials,
        'user_id',
        $this->getTable('customer/entity'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
);

$installer->endSetup();