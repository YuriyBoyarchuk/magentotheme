<?php

class TS_Testimonials_Block_Adminhtml_Testimonials extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct() {
        parent::_construct();

        $helper = Mage::helper('tstestimonials');
        $this->_blockGroup = 'tstestimonials';
        $this->_controller = 'adminhtml_testimonials';

        $this->_headerText = $helper->__('Testimonials Management');
    }

}