<?php

class TS_Testimonials_Block_Adminhtml_Testimonials_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $helper = Mage::helper('tstestimonials');
        $model  = Mage::registry('current_testimonials');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array(
                    'id' => $this->getRequest()->getParam('id')
                )),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $this->setForm($form);

        $fieldset = $form->addFieldset('testimonials_form', array('legend' => $helper->__('Testimonials Information')));

        $customers = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('firstname')->addAttributeToSelect('lastname');
        $customer_list = array();

        if (null === $model->getId()) {

            foreach ($customers as $customer) {
                $customer_list[$customer->getId()] = $customer->getName();
            }

            $fieldset->addField('customer', 'select', array(
                'values' => $customer_list,
                'label' => $helper->__('Customer'),
                'required' => true,
                'name'  => 'user_id'
            ));

        }

        $fieldset->addField('show_status', 'select', array(
            'values'    => array('1' => 'Yes', '0' => 'No'),
            'label'     => $helper->__('Show Status'),
            'name'      => 'show_status'
        ));

        $fieldset->addField('content', 'editor', array(
            'label' => $helper->__('Content'),
            'required' => true,
            'name' => 'content',
        ));

        if (null !== $model->getId()) {

            $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
            $fieldset->addField('created_at', 'datetime', array(
                'format' => $dateFormatIso,
                'input_format' => $dateFormatIso,
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
                'label' => $helper->__('Created At'),
                'name' => 'created_at',
                'time' => true,
            ));
        }

        $form->setUseContainer(true);

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        } else {
            $form->setValues($model->getData());
        }

        return parent::_prepareForm();
    }

}