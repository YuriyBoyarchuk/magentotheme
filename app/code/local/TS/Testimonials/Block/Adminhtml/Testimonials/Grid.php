<?php

class TS_Testimonials_Block_Adminhtml_Testimonials_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _prepareCollection() {
        $collection = Mage::getModel('tstestimonials/testimonials')->getCollection();
        $select = $collection->getSelect();

        $customerFirstNameAttr = Mage::getSingleton('customer/customer')->getResource()->getAttribute('firstname');
        $customerLastNameAttr  = Mage::getSingleton('customer/customer')->getResource()->getAttribute('lastname');
        $select->joinLeft(
                array('cusFirstnameTb' => $customerFirstNameAttr->getBackend()->getTable()),
                'main_table.user_id = cusFirstnameTb.entity_id AND cusFirstnameTb.attribute_id = '.$customerFirstNameAttr->getId(). ' AND cusFirstnameTb.entity_type_id = '.Mage::getSingleton('customer/customer')->getResource()->getTypeId(),
                array('cusFirstnameTb.value')
            );

        $select->joinLeft(
                array('cusLastnameTb' => $customerLastNameAttr->getBackend()->getTable()),
                'main_table.user_id = cusLastnameTb.entity_id AND cusLastnameTb.attribute_id = '.$customerLastNameAttr->getId(). ' AND cusLastnameTb.entity_type_id = '.Mage::getSingleton('customer/customer')->getResource()->getTypeId(),
                array('customer_name' => "CONCAT(cusFirstnameTb.value, ' ', cusLastnameTb.value)")
            );

        $table   = Mage::getSingleton('customer/customer')->getResource()->getTable('customer/entity');
        $select->join(array('ce'=>$table), 'ce.entity_id = user_id');

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $helper = Mage::helper('tstestimonials');

        $this->addColumn('id', array(
            'header' => $helper->__('Testimonials ID'),
            'index' => 'id'
        ));

        $this->addColumn('content', array(
            'header' => $helper->__('Content'),
            'index' => 'content',
            'type' => 'text',
        ));

        $this->addColumn('customer_name', array(
            'header'    => Mage::helper('adminhtml')->__('Customer Name'),
            'index'     => 'customer_name',
            'filter_condition_callback' => array($this, 'customerNameFilter'),
        ));

        $this->addColumn('email', array(
            'header' => $helper->__('Email'),
            'index'  => 'email',
            'type'   => 'text'
        ));

        $this->addColumn('show_status', array(
            'header' => $helper->__('Show Status'),
            'index'  => 'show_status',
            'type'    => 'options',
            'options' => array('1' => 'Yes', '0' => 'No')
        ));

        $this->addColumn('created_at', array(
            'header' => $helper->__('Created_at'),
            'index' => 'created_at',
            'type' => 'date',
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {

        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('testimonials');

        $this->getMassactionBlock()
            ->addItem('delete', array(
                'label' => $this->__('Delete'),
                'url'   => $this->getUrl('*/*/massDelete'),
            ))
            ->addItem('change_status', array(
                'label' => $this->__('Change status'),
                'url'   => $this->getUrl('*/*/massSetStatus', array('_current'=>true)),
                'additional' => array(
                    'visibility' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'values' => array('yes'=>'Yes', 'no'=>'No')
                    )
                )
            ));
        return $this;
    }

    public function getRowUrl($model) {

        return $this->getUrl('*/*/edit', array(
            'id' => $model->getId(),
        ));
    }

    public function customerNameFilter($collection, $column){
        $filterValue = $column->getFilter()->getValue();
        if(!is_null($filterValue)){
            $filterValue = trim($filterValue);
            $filterValue = preg_replace('/[\s]+/', ' ', $filterValue);

            $whereArr = array();
            $whereArr[] = $collection->getConnection()->quoteInto("cusFirstnameTb.value = ?", $filterValue);
            $whereArr[] = $collection->getConnection()->quoteInto("cusLastnameTb.value = ?", $filterValue);
            $whereArr[] = $collection->getConnection()->quoteInto("CONCAT(cusFirstnameTb.value, ' ', cusLastnameTb.value) = ?", $filterValue);
            $where = implode(' OR ', $whereArr);
            $collection->getSelect()->where($where);
        }
    }

}