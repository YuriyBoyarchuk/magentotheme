<?php

class TS_Testimonials_Block_Testimonials extends Mage_Core_Block_Template
{
    public function __construct() {

        $testimonials = Mage::getModel('tstestimonials/testimonials')->getCollection()
            ->addFieldToFilter('show_status', 1)
            ->setOrder('created_at', 'DESC');

        $this->setTestimonials($testimonials);

        $pager = new Mage_Page_Block_Html_Pager();
        $pager->setCollection($testimonials);

        $this->setChild('pager', $pager);
    }
}